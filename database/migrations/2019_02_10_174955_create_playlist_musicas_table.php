<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlaylistMusicasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('playlist_musicas', function(Blueprint $table)
		{
			$table->dateTime('dataplaylist_musica');
			$table->integer('playlists_id')->index('fk_playlist_musicas_playlists1_idx');
			$table->integer('musicas_id')->index('fk_playlist_musicas_musicas1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('playlist_musicas');
	}

}
