<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    protected $fillable = [
        'nomePlaylist', 'usuarios_id'
    ];
}
