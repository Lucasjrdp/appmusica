<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPermissoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('permissoes', function(Blueprint $table)
		{
			$table->foreign('playlists_id', 'fk_permissoes_playlists1')->references('id')->on('playlists')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('usuarios_id', 'fk_permissoes_usuarios1')->references('id')->on('usuarios')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('permissoes', function(Blueprint $table)
		{
			$table->dropForeign('fk_permissoes_playlists1');
			$table->dropForeign('fk_permissoes_usuarios1');
		});
	}

}
