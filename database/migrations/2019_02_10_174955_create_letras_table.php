<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLetrasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('letras', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('stringletra', 65535);
			$table->timestamp('dataletra')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('musicas_id')->index('fk_letras_musicas1_idx');
			$table->integer('usuarios_id')->index('fk_letras_usuarios1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('letras');
	}

}
