<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLetrasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('letras', function(Blueprint $table)
		{
			$table->foreign('musicas_id', 'fk_letras_musicas1')->references('id')->on('musicas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('usuarios_id', 'fk_letras_usuarios1')->references('id')->on('usuarios')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('letras', function(Blueprint $table)
		{
			$table->dropForeign('fk_letras_musicas1');
			$table->dropForeign('fk_letras_usuarios1');
		});
	}

}
