<?php
/**
 * Created by PhpStorm.
 * User: NUMBER ONE
 * Date: 10/02/2019
 * Time: 18:21
 */

namespace App\Api;

class ApiError
{

    public static function errorMessage($message, $code)
    {
        return [
            'data' => ['msg' => $message,
                'status' => $code]
        ];
    }

}