<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventoMusicasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('evento_musicas', function(Blueprint $table)
		{
			$table->text('detalhesevento_musica', 65535)->nullable();
			$table->integer('musicas_id')->index('fk_evento_musicas_musicas1_idx');
			$table->integer('eventos_id')->index('fk_evento_musicas_eventos1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('evento_musicas');
	}

}
