<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMusicasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('musicas', function(Blueprint $table)
		{
			$table->increments('id', true);
			$table->string('nomemusica', 250);
			$table->string('arquivomusica', 250)->nullable()->unique('arquivomusica_UNIQUE');
			$table->string('artistamusica', 250)->nullable();
			$table->string('albummusica')->nullable();
			$table->integer('viewsmusica');
			$table->timestamp('datamusica')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('usuarios_id')->index('fk_musicas_usuarios1_idx');
			$table->boolean('flativo')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('musicas');
	}

}
