<?php

namespace App\Http\Controllers\Api;

use App\Api\ApiError;
use App\Playlist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlaylistController extends Controller
{

    private $playlist;

    public function __construct(Playlist $playlist)
    {
        $this->playlist = $playlist;

    }

    public function index() {
        $data = ["data" => $this->playlist->all()];
        return response()->json($data);
    }

    public function show(Playlist $id) {
        $data = ["data" => $id];
        return response()->json($data);
    }

    public function nova(Request $request)
    {
        try {
            $playListData = $request->all();
            $this->playlist->create($playListData);

            return response()->json(["msg" => "Playlist Criada com Sucesso!"], 201);

        } catch (\Exception $e) {
            if (!config('app.debug')) {
                return response()->json( ApiError::errorMessage($e->getMessage()), 401);
            }

            return response()->json( ApiError::errorMessage("Ocorreu um erro ao criar a Playlist!", 401));
        }

    }

    public function update (Playlist $request, $id) {
        try {
            $playListData = $request->all();
            $playlist->find($id);
            $playlist->update($playListData);

            return response()->json(["msg" => "Playlist Atualizada com Sucesso!"], 201);

        } catch (\Exception $e) {
            if (!config('app.debug')) {
                return response()->json( ApiError::errorMessage($e->getMessage()), 401);
            }

            return response()->json( ApiError::errorMessage("Ocorreu um erro ao Atualizar a Playlist!", 401));
        }
    }


}
