<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlaylistsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('playlists', function(Blueprint $table)
		{
			$table->increments('id', true);
			$table->string('nomeplaylist')->unique('nomeplaylist_UNIQUE');
			$table->boolean('privadaplaylist')->default(0);
			$table->integer('viewsplaylist')->default(0);
			$table->string('capaplaylist', 200)->nullable();
			$table->string('linkplaylist')->nullable()->unique('linkplaylist_UNIQUE');
			$table->string('senhaplaylist')->nullable();
			$table->timestamps();
			$table->integer('usuarios_id')->index('fk_playlists_usuarios_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('playlists');
	}

}
