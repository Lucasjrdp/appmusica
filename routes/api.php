<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/ok', function () {
    return ["status" => true];
});

Route::prefix('')->namespace('Api')->name('api.')->group(function (){
    Route::prefix('/playlists')->group(function (){

        Route::get('/', 'PlaylistController@index')->name('index_playlists');
        Route::get('/{id}', 'PlaylistController@show')->name('single_playlists');
        Route::post('/', 'PlaylistController@nova')->name('nova_playlists');
        Route::put('/{id}', 'PlaylistController@update')->name('update_playlists');
    });

});