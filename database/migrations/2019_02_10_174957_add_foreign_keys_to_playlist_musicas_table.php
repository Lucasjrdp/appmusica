<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlaylistMusicasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('playlist_musicas', function(Blueprint $table)
		{
			$table->foreign('musicas_id', 'fk_playlist_musicas_musicas1')->references('id')->on('musicas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('playlists_id', 'fk_playlist_musicas_playlists1')->references('id')->on('playlists')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('playlist_musicas', function(Blueprint $table)
		{
			$table->dropForeign('fk_playlist_musicas_musicas1');
			$table->dropForeign('fk_playlist_musicas_playlists1');
		});
	}

}
